# About

This project was made because I kept copying and pasting the same code.
So I thought I just make it into a library that I can use across projects.

## Why is there only pulling of posts.

The reason is that getting a local copy of the site running was a pain.
Not to mention, for pretty much all of my projects I just need to pull posts from e621 or e926.

# Can I contribute?

Sure, just make sure that you are nice to others and that your code does not contain malware.
Your contributions must also not break the GNU General Public License or the rules of e621/926.
Being mean to anyone else can result in a ban on contributing.

Basically, don't be a dick and follow the rules.