﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

#endregion

namespace ESixSharp {
	public static class PostManager {
		public const string MAIN_SITE_POST       = "https://e621.net/posts.json";
		public const string MAIN_SITE_QUERY_POST = "https://e621.net/posts.json?tags={0}";

		public const string SISTER_SITE_POST       = "https://e926.net/posts.json";
		public const string SISTER_SITE_QUERY_POST = "https://e926.net/posts.json?tags={0}";

		private static readonly ESixManager manager = ESixManager.CurrentManager;

		/// <summary>
		///     This method gathers all posts on e621.net, this could take a lot of time.
		///     While gathering all posts, you will not be able to get other things.
		/// </summary>
		/// <remarks>
		///     Since this method is blocking, it is recommended to use <see cref="EnumerateAllPosts" /> instead.
		///     To search for posts, use the <see cref="GetPostsFromTags(string[])" /> method.
		/// </remarks>
		/// <returns>An array of <see cref="Post" />s.</returns>
		public static Post[] GetAllPosts() => EnumeratePostsFromLink( MAIN_SITE_POST ).ToArray();

		/// <summary>
		///     Well grab 320 posts per page for <paramref name="limit" /> of page cycles.
		/// </summary>
		/// <remarks>
		///     This method will grab all posts from pages, the only limiting factor the <paramref name="limit" /> parameter.
		///     <paramref name="limit" /> needs to either be -1 or between 1 (included) and <see cref="int.MaxValue" /> (included).
		///     Note, however, that the site most likely does not have 687,194,767,040 (320 posts per page *
		///     <see cref="int.MaxValue" />) of posts.
		///     This means that setting it to that high of a number means that it is effectively going to act the same as -1.
		/// </remarks>
		/// <param name="limit">Number of pages to go through.</param>
		/// <returns>An array of <see cref="Post" />s.</returns>
		public static Post[] GetAnyPosts( int limit ) => EnumeratePostsFromLink( MAIN_SITE_POST, limit ).ToArray();

		/// <summary>
		///     Will return <see cref="Post" />s from e621.net that contains the tags.
		///     While gathering all posts, you will not be able to interact with e621/e926.
		/// </summary>
		/// <remarks>
		///     Since this method is blocking, it is recommended to use <see cref="EnumeratePostsFromTags(string[])" /> instead.
		///     To gather all posts, please use either <see cref="GetAllPosts" /> (not recommended) or
		///     <see cref="EnumerateAllPosts" /> (recommended).
		///     However, this is no advisable since e621/926 contains thousands upon thousands of posts.
		///     <paramref name="limit" /> needs to either be -1 or between 1 (included) and <see cref="int.MaxValue" /> (included).
		///     Note, however, that the site most likely does not have 687,194,767,040 (320 posts per page *
		///     <see cref="int.MaxValue" />) of posts.
		///     This means that setting it to that high of a number means that it is effectively going to act the same as -1.
		/// </remarks>
		/// <param name="tags">A list of tags the post must meet.</param>
		/// <param name="limit">The number of pages to go through.</param>
		/// <returns>An array of <see cref="Post" />s.</returns>
		public static Post[] GetPostsFromTags( int limit = -1, params string[] tags ) {
			var queryURL = string.Format( MAIN_SITE_QUERY_POST, GenerateTagQuery( tags ) );

			return EnumeratePostsFromLink( queryURL, limit ).ToArray();
		}

		/// <summary>
		///     This method gathers all posts on e926.net, this could take a lot of time.
		/// </summary>
		/// <remarks>
		///     This post is blocking in the sense that no calls can be made to e926/621 during gathering.
		///     If you want to not block, then use <see cref="EnumerateAllSafePosts" /> instead.
		///     To search for posts, use the <see cref="GetPostsFromSafeTags(string[])" />.
		/// </remarks>
		/// <returns>An array of <see cref="Post" />s from e926.</returns>
		public static Post[] GetAllSafePosts() => EnumeratePostsFromLink( SISTER_SITE_POST ).ToArray();

		/// <summary>
		///     Will grab 320 posts per pages for <paramref name="limit" /> of page cycles.
		/// </summary>
		/// <remarks>
		///     This method will grab all posts from pages, the only limiting factor the <paramref name="limit" /> parameter.
		///     <paramref name="limit" /> needs to either be -1 or between 1 (included) and <see cref="int.MaxValue" /> (included).
		///     Note, however, that the site most likely does not have 687,194,767,040 (320 posts per page *
		///     <see cref="int.MaxValue" />) of posts.
		///     This means that setting it to that high of a number means that it is effectively going to act the same as -1.
		///     This method will only grab from e926.net and will not grab from e621.net.
		/// </remarks>
		/// <param name="limit">The number of pages to go through.</param>
		/// <returns>An array of <see cref="Post" />s that has been gathered.</returns>
		public static Post[] GetAnySafePosts( int limit ) => EnumeratePostsFromLink( SISTER_SITE_POST, limit ).ToArray();

		/// <summary>
		///     Will return <see cref="Post" />s from e926.net that contains the tags.
		/// </summary>
		/// <remarks>
		///     While gathering all posts, you will not be able to interact with e621/926.
		///     If you want to not block, then use <see cref="EnumeratePostsFromSafeTags(string[])" /> instead.
		///     To gather all posts, please use either <see cref="GetAllSafePosts" /> (not recommended) or
		///     <see cref="EnumerateAllSafePosts" /> (recommended).
		///     However, this is not advisable since e621/926 contains thousands upon thousands of posts.
		///     <paramref name="limit" /> needs to either be -1 or between 1 (included) and <see cref="int.MaxValue" /> (included).
		///     Note, however, that the site most likely does not have 687,194,767,040 (320 posts per page *
		///     <see cref="int.MaxValue" />) of posts.
		///     This means that setting it to that high of a number means that it is effectively going to act the same as -1.
		/// </remarks>
		/// <param name="tags">A list of tags the post must meet.</param>
		/// <param name="limit">The number of times to pull the site.</param>
		/// <returns>An array of <see cref="Post" />s.</returns>
		public static Post[] GetPostsFromSafeTags( int limit = -1, params string[] tags ) {
			var queryURL = string.Format( SISTER_SITE_QUERY_POST, GenerateTagQuery( tags ) );

			return EnumeratePostsFromLink( queryURL, limit ).ToArray();
		}

		/// <summary>
		///     This method will allow enumerating through e621.net's entire database of posts.
		///     It is recommended to use this method as it won't be blocking for pulling data.
		///     The reason is that it is unknown how long the caller will take to process data.
		/// </summary>
		/// <returns>A <see cref="Post" /> that it got from e621.net.</returns>
		public static IEnumerable<Post> EnumerateAllPosts() => EnumeratePostsFromLink( MAIN_SITE_POST );

		/// <summary>
		///     Will grab 320 posts per pages for <paramref name="limit" /> of page cycles.
		/// </summary>
		/// <remarks>
		///     <paramref name="limit" /> needs to either be -1 or between 1 (included) and <see cref="int.MaxValue" /> (included).
		///     Note, however, that the site most likely does not have 687,194,767,040 (320 posts per page *
		///     <see cref="int.MaxValue" />) of posts.
		///     This means that setting it to that high of a number means that it is effectively going to act the same as -1.
		/// </remarks>
		/// <param name="limit"></param>
		/// <returns></returns>
		public static IEnumerable<Post> EnumerateAnyPosts( int limit ) => EnumeratePostsFromLink( MAIN_SITE_POST, limit );

		/// <summary>
		///     Will send a request to the site using the tags for querying for the site.
		/// </summary>
		/// <remarks>
		///     To gather all posts, use <see cref="EnumerateAllPosts" />.
		///     <paramref name="limit" /> needs to either be -1 or between 1 (included) and <see cref="int.MaxValue" /> (included).
		///     Note, however, that the site most likely does not have 687,194,767,040 (320 posts per page *
		///     <see cref="int.MaxValue" />) of posts.
		///     This means that setting it to that high of a number means that it is effectively going to act the same as -1.
		/// </remarks>
		/// <param name="tags">A list of tags the post must meet.</param>
		/// <param name="limit">The number of pages to cycle through.</param>
		/// <returns><see cref="Post" />s that meet the criteria set within the tags.</returns>
		public static IEnumerable<Post> EnumeratePostsFromTags( int limit = -1, params string[] tags ) {
			var queryURL = string.Format( MAIN_SITE_QUERY_POST, GenerateTagQuery( tags ) );

			return EnumeratePostsFromLink( queryURL, limit );
		}

		/// <summary>
		///     This method will allow enumerating through e926.net's entire database of posts.
		///     It is recommended to use this method as it won't be blocking for interacting with the site.
		/// </summary>
		/// <returns>A <see cref="Post" /> that it got from e621.net.</returns>
		public static IEnumerable<Post> EnumerateAllSafePosts() => EnumeratePostsFromLink( SISTER_SITE_POST );

		/// <summary>
		///     Will grab 320 posts per pages for <paramref name="limit" /> of page cycles.
		/// </summary>
		/// <remarks>
		///     <paramref name="limit" /> needs to either be -1 or between 1 (included) and <see cref="int.MaxValue" /> (included).
		///     Note, however, that the site most likely does not have 687,194,767,040 (320 posts per page *
		///     <see cref="int.MaxValue" />) of posts.
		///     This means that setting it to that high of a number means that it is effectively going to act the same as -1.
		/// </remarks>
		/// <param name="limit">The number of pages to go through.</param>
		/// <returns>An <see cref="IEnumerable{T}" /> of posts.</returns>
		public static IEnumerable<Post> EnumerateAnySafePosts( int limit ) => EnumeratePostsFromLink( SISTER_SITE_POST, limit );

		/// <summary>
		///     Will send a request to the site using the tags for querying for the site.
		/// </summary>
		/// <remarks>
		///     To gather all posts, use <see cref="EnumerateAllSafePosts" />
		///     <paramref name="limit" /> needs to either be -1 or between 1 (included) and <see cref="int.MaxValue" /> (included).
		///     Note, however, that the site most likely does not have 687,194,767,040 (320 posts per page *
		///     <see cref="int.MaxValue" />) of posts.
		///     This means that setting it to that high of a number means that it is effectively going to act the same as -1.
		/// </remarks>
		/// <param name="tags">A list of tags the post must meet.</param>
		/// <param name="limit">The number of times to pull from the size.</param>
		/// <returns>An <see cref="IEnumerable{T}" /> of posts that have been deserialized.</returns>
		public static IEnumerable<Post> EnumeratePostsFromSafeTags( int limit = -1, params string[] tags ) {
			var queryURL = string.Format( SISTER_SITE_QUERY_POST, GenerateTagQuery( tags ) );

			return EnumeratePostsFromLink( queryURL, limit );
		}

		public static Post GetPostFromLink( string link ) => GetPostFromLink( new Uri( link ) );

		public static Post GetPostFromLink( Uri link ) {
			string json;
			Uri    url;

			// Want to remove this to not clutter up the rest of the code.
			{
				var uriBuilder = new UriBuilder( link ) {
					Query = string.Empty,
				};

				if ( !link.ToString().Contains( ".json", StringComparison.OrdinalIgnoreCase ) ) {
					uriBuilder.Path += ".json";
				}

				url = uriBuilder.Uri;
			}

			lock ( manager.GlobalLockObject ) {
				using var webClient = manager.CreateWebClient();

				manager.DelayRequest();

				json = webClient.DownloadString( url );
			}

			json = FixJson( json );

			return JsonConvert.DeserializeObject<Post>( json );
		}

		/* This method is used to string the tags into a single string.
		 * Since there are numerous method that use this, it is placed here. */
		private static string GenerateTagQuery( string[] tags ) {
			var builder = new StringBuilder();

			for ( var i = 0; i < tags.Length; ++i ) {
				builder.Append( tags[i] );

				if ( i < tags.Length - 1 ) {
					builder.Append( '+' );
				}
			}

			return builder.ToString();
		}

		/* The site (e621/926) returns a json that Newtonsoft.Json doesn't like.
		 * So this method fixes that and returns the fixed string. */
		private static string FixJson( string json ) {
			var start = json.IndexOf( ':' ) + 1;
			var end   = json.LastIndexOf( '}' );

			return json.Substring( start, end - start );
		}

		// The main method that is used to enumerate all of the posts.
		private static IEnumerable<Post> EnumeratePostsFromLink( string postUrl, int limit = -1 ) {
			var    url   = postUrl;
			var    count = 0u;
			Post[] posts;

			if ( !url.Contains( "limit=" ) ) {
				postUrl = url.Contains( "?" ) ? $"{url}&limit=320" : $"{url}?limit=320";
			}

			if ( limit != -1 && !( limit > 0 ) ) {
				throw new ArgumentException( $"Limit must either be -1 or [1, {int.MaxValue}].", nameof( limit ) );
			}

			do {
				string json;

				lock ( manager.GlobalLockObject ) {
					manager.DelayRequest();

					using var webClient = manager.CreateWebClient();

					json = webClient.DownloadString( url );
				}

				json = FixJson( json );

				posts = JsonConvert.DeserializeObject<Post[]>( json );

				if ( posts is null || posts.Length == 0 ) {
					break;
				}

				foreach ( var post in posts ) {
					if ( post.File.Hidden ) {
						continue;
					}

					yield return post;
				}

				url = $"{postUrl}&page=b{posts?[^1].ID}";
			} while ( ( /* Checks limit bounds. */ limit > 0 && ++count < limit || limit == -1 )
					  && posts is Post[] && posts.Length > 0 );
		}
	}
}